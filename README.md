# Exercise

## The story

I open my eyes. Slowly. Painfully. My mouth is dry, like sandpaper dry. Too much light. Too much light coming from the window!
Wait, that is not my window. Those drapes... This is not my bedroom. This is not my house!

The realization kicks in, and I get up, fast. Oh God, that was a mistake. A headache like a knife goes through my brain, slicing whatever feeling of alertness I had.

I try to open the door. No luck. The door is closed. Try the window... and also closed. Looks like I am in a building. A tall one. No escape here.

Take a look to the city. I know this city! Is Munich!
How the hell did I get here??!!

I take a look around, and I see a desk. Top of that desk are train tickets.

One for London-Paris, one for Amsterdam-Munich, another for Madrid-London and one for Paris-Amsterdam.
Tickets do not have dates, anything. Just origin and destination.

Can I retrace my steps to find out my trip?
What is the correct order of the tickets?


## The task

1. Write a program in whatever language you want to solve the questions above.
2. Has to have tests. :)
3. Also, it should contain an executable for the command line.
4. Share it!
