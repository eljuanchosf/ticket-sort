package ticket

import "fmt"

type Ticket struct {
	Origin      string `json:"origin"`
	Destination string `json:"destination"`
}

func (t *Ticket) ID() string {
	return fmt.Sprintf("%s->%s", t.Origin, t.Destination)
}
