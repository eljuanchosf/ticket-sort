package ticket

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTicket_ID(t *testing.T) {
	ticket := Ticket{
		Origin:      "Origin",
		Destination: "Destination",
	}

	assert.Equal(t, "Origin->Destination", ticket.ID())
}
