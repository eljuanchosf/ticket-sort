package trip

import (
	"bottle-pay-test/internal/ticket"
	"testing"

	"github.com/stretchr/testify/assert"
)

var trip = Trip{
	[]ticket.Ticket{
		{
			Origin:      "Paris",
			Destination: "Amsterdam",
		},
		{
			Origin:      "Madrid",
			Destination: "London",
		},
		{
			Origin:      "Amsterdam",
			Destination: "Munich",
		},
		{
			Origin:      "London",
			Destination: "Paris",
		},
	},
}

func TestFindStartingPointTicket(t *testing.T) {
	startTicket := trip.getStartingPointTicket()
	assert.Equal(t, "Madrid->London", startTicket.ID())
}

func TestGetNextCity(t *testing.T) {
	fistTicket := trip.getStartingPointTicket()
	nextTicket := trip.findNextTicket(fistTicket)
	assert.Equal(t, "London->Paris", nextTicket.ID())
}

func TestGetTripFromTickets(t *testing.T) {
	expectedTrip := []string{"Madrid->London", "London->Paris", "Paris->Amsterdam", "Amsterdam->Munich"}
	actualTrip := trip.GetTicketsInOrder()
	assert.Equal(t, expectedTrip, actualTrip)
}
