package trip

import (
	"bottle-pay-test/internal/ticket"
	"encoding/json"
)

type Trip struct {
	Tickets []ticket.Ticket `json:"tickets"`
}

func New(tickets []byte) (*Trip, error) {
	trip := Trip{}
	err := json.Unmarshal(tickets, &trip)
	if err != nil {
		return nil, err
	}
	return &trip, nil
}

func (t *Trip) GetTicketsInOrder() []string {
	var actualTrip []string
	ticket := t.getStartingPointTicket()
	if ticket == nil {
		return nil
	}
	for ok := true; ok; ok = ticket != nil {
		actualTrip = append(actualTrip, ticket.ID())
		ticket = t.findNextTicket(ticket)
	}
	return actualTrip
}

func (t *Trip) getStartingPointTicket() *ticket.Ticket {
	for _, ticket := range t.Tickets {
		if t.originIsNotInDestinations(ticket.Origin) {
			return &ticket
		}
	}
	return nil
}

func (t *Trip) findNextTicket(startingTicket *ticket.Ticket) *ticket.Ticket {
	for _, ticket := range t.Tickets {
		if ticket.Origin == startingTicket.Destination {
			return &ticket
		}
	}
	return nil
}

func (t *Trip) originIsNotInDestinations(city string) bool {
	for _, ticket := range t.Tickets {
		if ticket.Destination == city {
			return false
		}
	}
	return true
}
