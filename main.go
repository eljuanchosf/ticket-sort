package main

import (
	"bottle-pay-test/internal/trip"
	"flag"
	"fmt"
	"log"
	"os"
)

func main() {
	tickets := flag.String("tickets", "", "JSON file containing the tickets")

	flag.Parse()

	if tickets == nil {
		log.Fatal("you need to specify the tickets file or object")
	}

	_, err := os.Stat(*tickets)
	if err != nil {
		log.Fatalf("%s is not a file", *tickets)
	}
	ticketsObject, err := os.ReadFile(*tickets)
	if err != nil {
		log.Fatalf("cannot load file %s", *tickets)
	}
	trip, err := trip.New(ticketsObject)
	if err != nil {
		log.Fatalf("Error while parsing trip: %v", err)
	}
	ticketsInOrder := trip.GetTicketsInOrder()
	fmt.Printf("Trip: %v\n", ticketsInOrder)
}
